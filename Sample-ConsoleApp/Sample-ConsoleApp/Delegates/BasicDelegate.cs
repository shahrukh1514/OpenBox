﻿using System;

namespace Sample_ConsoleApp.Delegates
{
    public class BasicDelegate
    {
        public static void MyFunc()
        {
            Console.WriteLine("I was called by delegate ...");
        }

    }
}
