﻿namespace Sample_ConsoleApp.Delegates
{
    class StaticDelegates
    {
        // Declare a delegate that takes a single string parameter
        // and has no return type.
        public delegate void LogHandler(string message);

        // The use of the delegate is just like calling a function directly,
        // though we need to add a check to see if the delegate is null
        // (that is, not pointing to a function) before calling the function.
        public static void Process(LogHandler logHandler)
        {
            logHandler?.Invoke("Process() begin");

            logHandler?.Invoke("Process() end");
        }
    }
}
