﻿using Sample_ConsoleApp.Delegates;
using Sample_ConsoleApp.Publisher;
using Sample_ConsoleApp.Subscriber;
using Sample_ConsoleApp.Utils;
using System;

namespace Sample_ConsoleApp
{
    class Program
    {
        // Declaration
        public delegate void SimpleDelegate();

        class TestDelegate
        {
            public static void Main()
            {
                //InvokeBasicDelegate();
                //InvokeStaticDelegate();
                //InvokeInstanceDelegate();
                //InvokeMulticastDelegates();
                //InvokeEvent();
                InvokeClock();

                Console.ReadKey();
            }
            
            #region DelegateInvocation
            private static void InvokeStaticDelegate()
            {

                // Crate an instance of the delegate, pointing to the logging function.
                // This delegate will then be passed to the Process() function.
                StaticDelegates.LogHandler myLogger = new StaticDelegates.LogHandler(Logger);
                StaticDelegates.Process(myLogger);
            }

            private static void InvokeBasicDelegate()
            {
                // Instantiation
                SimpleDelegate simpleDelegate = new SimpleDelegate(BasicDelegate.MyFunc);

                // Invocation
                simpleDelegate();
            }
            
            private static void InvokeInstanceDelegate()
            {
                FileLogger fl = new FileLogger("process.log");

                InstanceDelegates idObj = new InstanceDelegates();

                // Crate an instance of the delegate, pointing to the Logger()
                // function on the fl instance of a FileLogger.
                InstanceDelegates.LogHandler myLogger = new InstanceDelegates.LogHandler(fl.Logger);
                idObj.Process(myLogger);
                fl.Close();
            }
            private static void InvokeMulticastDelegates()
            {
                FileLogger fl = new FileLogger("process.log");

                MulticastDelegates mcdObj = new MulticastDelegates();

                // Crate an instance of the delegates, pointing to the static
                // Logger() function defined in the TestApplication class and
                // then to member function on the fl instance of a FileLogger.
                MulticastDelegates.LogHandler myLogger = null;
                myLogger += new MulticastDelegates.LogHandler(Logger);
                myLogger += new MulticastDelegates.LogHandler(fl.Logger);

                //myLogger = null;
                //MulticastDelegates.LogHandler log1 = new MulticastDelegates.LogHandler(Logger);
                //MulticastDelegates.LogHandler log2 = new MulticastDelegates.LogHandler(fl.Logger);
                //myLogger = log1 + log2;


                mcdObj.Process(myLogger);
                fl.Close();
            }
            
            private static void InvokeEvent()
            {
                FileLogger fl = new FileLogger("process.log");
                BasicEvent beObj = new BasicEvent();

                // Subscribe the Functions Logger and fl.Logger
                //beObj.Log += new BasicEvent.LogHandler(Logger);
                //beObj.Log += new BasicEvent.LogHandler(fl.Logger);

                beObj.Log += Logger;
                beObj.Log += fl.Logger;

                //beObj.Log = null;
                // The Event will now be triggered in the Process() Method
                beObj.Process();

                fl.Close();
            }

            private static void InvokeClock()
            {
                // Create a new clock
                Clock theClock = new Clock();

                // Create the display and tell it to
                // subscribe to the clock just created
                DisplayClock dc = new DisplayClock();
                dc.Subscribe(theClock);

                // Create a Log object and tell it
                // to subscribe to the clock
                LogClock lc = new LogClock();
                lc.Subscribe(theClock);

                // Get the clock started
                theClock.Run();
            }
            #endregion

            #region HelperFunctions
            // Static Function: To which is used in the Delegate. To call the Process()
            // function, we need to declare a logging function: Logger() that matches
            // the signature of the delegate.
            static void Logger(string s)
            {
                Console.WriteLine(s);
            }

            #endregion
        }
    }
}
