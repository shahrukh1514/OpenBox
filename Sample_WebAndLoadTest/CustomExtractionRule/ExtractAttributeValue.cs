﻿using Microsoft.VisualStudio.TestTools.WebTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace CustomExtractionRule
{
    [DisplayName("Extract Attribute Value")]
    [Description("Extracts the value of any given attribute/element combination")]
    public class ExtractAttributeValue : ExtractionRule
    {
        [DisplayName("Element ID")]
        [Description("Unique ID of the element that needs to be processed for validation")]
        public string ElementId { get; set; }

        [DisplayName("Attribute Name")]
        [Description("The name of the attribute whose value needs to be extracted")]
        public string AttributeName { get; set; }

        public override void Extract(object sender, ExtractionEventArgs e)
        {
            if (string.IsNullOrEmpty(ElementId))
            {
                Fail(e, "Cannot find element with the provided ID");
                return;
            }

            if (string.IsNullOrEmpty(AttributeName))
            {
                Fail(e, "Cannot find attribute with the provided name");
                return;
            }

            ExtractValue(e);
        }

        private void ExtractValue(ExtractionEventArgs e)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(e.Response.BodyString);

            var element = doc.GetElementbyId(ElementId);

            if (element == null)
            {
                Fail(e, string.Format("Could not locate element with id '{0}' in the response", ElementId));
                return;
            }

            ExtractValue(e, element);
        }

        private void ExtractValue(ExtractionEventArgs e, HtmlNode element)
        {
            if (element.Attributes == null || element.Attributes.Count < 1)
            {
                Fail(e, "There are no attributes for the specified element");
                return;
            }

            if (element.Attributes.Where(b => b.Name.Equals(AttributeName)).FirstOrDefault() == null)
            {
                Fail(e, string.Format("Could not locate attribute name {0} inside the element with id {1}", AttributeName, ElementId));
                return;
            }

            e.WebTest.Context.Add(ContextParameterName, element.Attributes.Where(b => b.Name.Equals(AttributeName)).FirstOrDefault().Value);

            Pass(e, "The value has been extracted");
        }

        private void Pass(ExtractionEventArgs e, string msg)
        {
            e.Success = true;
            e.Message = msg;
        }

        private void Fail(ExtractionEventArgs e, string msg)
        {
            e.Success = false;
            e.Message = msg;
        }
    }
}
