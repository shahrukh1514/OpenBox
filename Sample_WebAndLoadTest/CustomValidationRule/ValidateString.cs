﻿using Microsoft.VisualStudio.TestTools.WebTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace CustomValidationRule
{
    [DisplayName("Numeric Validation")]
    [Description("Validates whether the value is numeric or not")]
    public class ValidateString : ValidationRule
    {
        [DisplayName("Element ID")]
        [Description("Unique ID of the element that needs to be processed for validation")]
        public string ElementId { get; set; }
        
        //[DisplayName("Value to Validate")]
        //[Description("The value which will be used to verify if its an integer value")]
        //public string ValueToValidate { get; set; }
        public override void Validate(object sender, ValidationEventArgs e)
        {
            if (string.IsNullOrEmpty(ElementId))
            {
                Fail(e, "Cannot find element with the provided ID");
                return;
            }

            //if (string.IsNullOrEmpty(ValueToValidate))
            //{
            //    Fail(e, "You have not provided value to validate");
            //    return;
            //}

            ValidateValue(e);
        }

        private void ValidateValue(ValidationEventArgs e)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(e.Response.BodyString);

            var element = doc.GetElementbyId(ElementId);

            if (element == null)
            {
                Fail(e, string.Format("Could not locate element with id '{0}' in the response", ElementId));
                return;
            }

            ValidateValue(e, element);
        }

        private void ValidateValue(ValidationEventArgs e, HtmlNode element)
        {
            if (string.IsNullOrEmpty(element.InnerText))
            {
                Fail(e, "The value to validate is empty");
                return;
            }

            int s;
            if (!int.TryParse(element.InnerText, out s))
            {
                Fail(e, "The value is not of Integer type");
                return;
            }

            Pass(e, "The value provided is an Integer");
        }

        private void Pass(ValidationEventArgs e, string msg)
        {
            e.IsValid = true;
            e.Message = msg;
        }

        private void Fail(ValidationEventArgs e, string msg)
        {
            e.IsValid = false;
            e.Message = msg;
        }
    }
}
