﻿using Microsoft.VisualStudio.TestTools.WebTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebRequestPlugin
{
    public class AutomateLogin : WebTestPlugin
    {
        public override void PreWebTest(object sender, PreWebTestEventArgs e)
        {
            e.WebTest.Context.Add("username", "admin");
            e.WebTest.Context.Add("password", "123456");

            base.PreWebTest(sender, e);
        }
    }
}
